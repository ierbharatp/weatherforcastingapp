package in.bharat.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author ErBharatp
 *
 */
@SpringBootApplication
public class WeatherForcastingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherForcastingApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
