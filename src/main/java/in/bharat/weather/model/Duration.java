package in.bharat.weather.model;

/**
 * 
 * @author ErBharatp
 *
 */
public enum Duration {
	Daily(24);

	int value;

	Duration(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
