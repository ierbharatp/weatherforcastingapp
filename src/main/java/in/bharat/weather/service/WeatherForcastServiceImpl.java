package in.bharat.weather.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.bharat.weather.common.WeatherBitUtil;
import in.bharat.weather.model.Duration;
import in.bharat.weather.model.Forecast;
import in.bharat.weather.model.Weather;

/**
 * 
 * @author ErBharatp
 *
 */
@Component
public class WeatherForcastServiceImpl implements WeatherForcastService {

	@Autowired
	private WeatherBitUtil weatherBitHelper;

	@Override
	public Forecast getHourlyForecastForZipCode(String countryCode, String zipCode, String tempOption) {
		Forecast forecast = weatherBitHelper.getHourlyForecast(countryCode, zipCode, Duration.Daily);
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(forecast.getTimezone()));
		List<Weather> weatherList = forecast.getWeatherList().stream()
				.filter(weather -> weather.getLocalDateTime().toLocalDate().isAfter(localDateTime.toLocalDate()))
				.collect(Collectors.toList());
		Weather weather;
		forecast.setWeatherList(weatherList);
		switch (tempOption.toLowerCase()) {
		case "coolest":
			weather = Collections.min(weatherList, Comparator.comparing(Weather::getTemperature));
			forecast.setCoolestHourOfDay(weather.getLocalDateTime());
			break;
		case "hottest":
			weather = Collections.max(weatherList, Comparator.comparing(Weather::getTemperature));
			forecast.setHottestHourOfDay(weather.getLocalDateTime());
			forecast.setWeatherList(weatherList);
			break;
		default:
			weather = Collections.min(weatherList, Comparator.comparing(Weather::getTemperature));
			forecast.setWeatherList(weatherList);
			forecast.setCoolestHourOfDay(weather.getLocalDateTime());
			break;
		}

		return forecast;
	}
}
