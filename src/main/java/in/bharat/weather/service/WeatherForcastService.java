package in.bharat.weather.service;

import org.springframework.stereotype.Service;

import in.bharat.weather.model.Forecast;

/**
 * 
 * @author ErBharatp
 *
 */
@Service
public interface WeatherForcastService {

	Forecast getHourlyForecastForZipCode(String countryCode, String zipCode, String tempOption);

}
